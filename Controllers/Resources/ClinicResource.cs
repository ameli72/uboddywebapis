using System;

namespace uBoddy.Controllers.Resources
{
    public class ClinicResource
    {
        public int Id { get; set; }

        public DateTime CreatedTS { get; set; }
        public DateTime LastUpdatedTS { get; set; }


        public string BusinessName { get; set; }

        public string CorporateId { get; set; }

        public string PhoneNumber { get; set; }


        public Byte[] CoverImage { get; set; }

        public Byte[] Logo { get; set; }



        public string Intro { get; set; }

        public bool Deactivated { get; set; }
    }
}