namespace uBoddy.Controllers.Resources
{
    public class TreatmentCategoryResource : KeyValuePairResource
    {
        public KeyValuePairResource CategoryType { get; set; }
    }
}