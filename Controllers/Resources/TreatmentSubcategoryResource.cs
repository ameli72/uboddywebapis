namespace uBoddy.Controllers.Resources
{
    public class TreatmentSubcategoryResource: KeyValuePairResource
    {
        public KeyValuePairResource TreatmentCategory { get; set; }
    }
}