using System;

namespace uBoddy.Controllers.Resources
{
    public class EmployeeResource
    {
        public int Id { get; set; }

        public DateTime CreatedTS { get; set; }
        public DateTime LastUpdatedTS { get; set; }


        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public bool IsTemporaryPassword { get; set; }


        public string FullName { get; set; }

        public bool Administrator { get; set; }

        public string UBoddyId { get; set; }

        public Byte[] ProfilePicture { get; set; }


        public bool Deactivated { get; set; }



        public ClinicResource Clinic { get; set; }

        public string Token { get; set; }
    }
}