using System;

namespace uBoddy.Controllers.Resources
{
    public class TreatmentResource
    {
        public int Id { get; set; }

        public DateTime CreatedTS { get; set; }
        public DateTime LastUpdatedTS { get; set; }


        public ClinicResource Clinic { get; set; }

        public KeyValuePairResource TreatmentSubcategory { get; set; }


        public uint MaxClients { get; set; }


        public string Title { get; set; }

        public string Description { get; set; }

        public bool Deactivated { get; set; }
    }
}