﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using uBoddy.Controllers.Resources;
using uBoddy.Core;
using uBoddy.Core.Helpers;
using uBoddy.Core.Models;

namespace uBoddy.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeeController : BaseController
    {
        private readonly IOptions<AppSettings> appSettings;
        private readonly ILogger<EmployeeController> logger;

        public EmployeeController(IOptions<AppSettings> appSettings, IMapper mapper, IUBoddyRepository repository, IUnitOfWork unitOfWork, ILogger<EmployeeController> logger) :
            base(mapper, repository, unitOfWork)
        {
            this.appSettings = appSettings;
            this.logger = logger;
        }





        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] EmployeeLoginRequest request)
        {
            var employee = await repository.Login(request);
            if (employee == null)
            {
                ModelState.AddModelError("Login Attempt Failed", "Wrong username or password");
                return BadRequest(ModelState);
            }

            //TODO: Verify that their email address has been verified

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Value.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, employee.Id.ToString()),
                    new Claim(ClaimTypes.Name, employee.FullName),
                    new Claim(ClaimTypes.Role, "Employee")
                }),
                Expires = DateTime.UtcNow.AddHours(16),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            employee.Token = tokenHandler.WriteToken(token);


            var result = mapper.Map<Employee, EmployeeResource>(employee);
            return Ok(result);
        }

        // [AllowAnonymous]
        // [HttpPost("forgotpassword")]
        // public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequest request)
        // {
        //     if (!await repository.ForgotPassword(request))
        //     {
        //         ModelState.AddModelError("Reset Password Attempt", "Wrong username");
        //         return BadRequest(ModelState);
        //     }

        //     await unitOfWork.CompleteAsync();

        //     return Ok();
        // }
    }
}
