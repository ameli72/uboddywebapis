using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using uBoddy.Core;

namespace uBoddy.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IMapper mapper;
        protected readonly IUBoddyRepository repository;
        protected readonly IUnitOfWork unitOfWork;


        public BaseController(IMapper mapper, IUBoddyRepository repository, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.repository = repository;
            this.unitOfWork = unitOfWork;
        }


        protected int GetEmployeeId()
        {
            var token = this.Request.Headers["Authorization"][0].Replace("Bearer ", "");
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = handler.ReadToken(token) as JwtSecurityToken;

            return int.Parse(tokenS.Claims.First(claim => claim.Type == "nameid").Value);
        }
    }
}