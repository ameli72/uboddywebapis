namespace uBoddy.Controllers
{
    public class EmployeeLoginRequest
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}