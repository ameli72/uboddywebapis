﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace uBoddy.Migrations
{
    public partial class InitialModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CategoryTypes",
                columns: table => new
                {
                    CategoryTypeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryTypes", x => x.CategoryTypeId);
                });

            migrationBuilder.CreateTable(
                name: "Clinics",
                columns: table => new
                {
                    ClinicId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedTS = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    LastUpdatedTS = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    BusinessName = table.Column<string>(maxLength: 128, nullable: false),
                    CorporateId = table.Column<string>(maxLength: 16, nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 32, nullable: false),
                    CoverImage = table.Column<byte[]>(nullable: true),
                    Logo = table.Column<byte[]>(nullable: true),
                    Intro = table.Column<string>(maxLength: 1024, nullable: true),
                    Deactivated = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clinics", x => x.ClinicId);
                });

            migrationBuilder.CreateTable(
                name: "TreatmentCategories",
                columns: table => new
                {
                    TreatmentCategoryId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    CategoryTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TreatmentCategories", x => x.TreatmentCategoryId);
                    table.ForeignKey(
                        name: "FK_TreatmentCategories_CategoryTypes_CategoryTypeId",
                        column: x => x.CategoryTypeId,
                        principalTable: "CategoryTypes",
                        principalColumn: "CategoryTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedTS = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    LastUpdatedTS = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    EmailAddress = table.Column<string>(maxLength: 256, nullable: false),
                    Password = table.Column<string>(maxLength: 256, nullable: false),
                    IsTemporaryPassword = table.Column<bool>(nullable: false),
                    FullName = table.Column<string>(maxLength: 128, nullable: false),
                    Administrator = table.Column<bool>(nullable: false),
                    UBoddyId = table.Column<string>(maxLength: 16, nullable: true),
                    ProfilePicture = table.Column<byte[]>(nullable: true),
                    Deactivated = table.Column<bool>(nullable: false),
                    ClinicId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeId);
                    table.ForeignKey(
                        name: "FK_Employees_Clinics_ClinicId",
                        column: x => x.ClinicId,
                        principalTable: "Clinics",
                        principalColumn: "ClinicId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TreatmentSubcategories",
                columns: table => new
                {
                    TreatmentSubcategoryId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    TreatmentCategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TreatmentSubcategories", x => x.TreatmentSubcategoryId);
                    table.ForeignKey(
                        name: "FK_TreatmentSubcategories_TreatmentCategories_TreatmentCategoryId",
                        column: x => x.TreatmentCategoryId,
                        principalTable: "TreatmentCategories",
                        principalColumn: "TreatmentCategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Treatments",
                columns: table => new
                {
                    TreatmentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedTS = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    LastUpdatedTS = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    ClinicId = table.Column<int>(nullable: false),
                    TreatmentSubcategoryId = table.Column<int>(nullable: false),
                    MaxClients = table.Column<long>(nullable: false),
                    Title = table.Column<string>(maxLength: 128, nullable: false),
                    Description = table.Column<string>(maxLength: 512, nullable: false),
                    Deactivated = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Treatments", x => x.TreatmentId);
                    table.ForeignKey(
                        name: "FK_Treatments_Clinics_ClinicId",
                        column: x => x.ClinicId,
                        principalTable: "Clinics",
                        principalColumn: "ClinicId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Treatments_TreatmentSubcategories_TreatmentSubcategoryId",
                        column: x => x.TreatmentSubcategoryId,
                        principalTable: "TreatmentSubcategories",
                        principalColumn: "TreatmentSubcategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employees_ClinicId",
                table: "Employees",
                column: "ClinicId");

            migrationBuilder.CreateIndex(
                name: "IX_TreatmentCategories_CategoryTypeId",
                table: "TreatmentCategories",
                column: "CategoryTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Treatments_ClinicId",
                table: "Treatments",
                column: "ClinicId");

            migrationBuilder.CreateIndex(
                name: "IX_Treatments_TreatmentSubcategoryId",
                table: "Treatments",
                column: "TreatmentSubcategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_TreatmentSubcategories_TreatmentCategoryId",
                table: "TreatmentSubcategories",
                column: "TreatmentCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Treatments");

            migrationBuilder.DropTable(
                name: "Clinics");

            migrationBuilder.DropTable(
                name: "TreatmentSubcategories");

            migrationBuilder.DropTable(
                name: "TreatmentCategories");

            migrationBuilder.DropTable(
                name: "CategoryTypes");
        }
    }
}
