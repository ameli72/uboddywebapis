﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace uBoddy.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Clinics (BusinessName, CorporateId, PhoneNumber, Deactivated) VALUES ('My Clinic', '1234', '+356 9929 5625', 'false')");


            migrationBuilder.Sql("INSERT INTO Employees (EmailAddress, Password, FullName, ClinicId, IsTemporaryPassword, Administrator, Deactivated) VALUES ('ameli72@hotmail.com', 'abc123', 'Andrew Meli', 1, 'false', 'true', 'false')");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Employees");
            migrationBuilder.Sql("DELETE FROM Clinics");
        }
    }
}
