using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using uBoddy.Controllers;
using uBoddy.Core;
using uBoddy.Core.Models;

namespace uBoddy.Persistence
{
    public class UBoddyRepository : IUBoddyRepository
    {
        private readonly UBoddyDBContext context;

        public UBoddyRepository(UBoddyDBContext context)
        {
            this.context = context;
        }


        public async Task<Employee> Login(EmployeeLoginRequest request)
        {
            var employee = await context.Employees
                .Include("Clinic")
                .SingleOrDefaultAsync(e => e.EmailAddress == request.EmailAddress &&
                    e.Password == request.Password);

            return employee;
        }
    }
}