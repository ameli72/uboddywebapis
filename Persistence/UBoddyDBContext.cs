using Microsoft.EntityFrameworkCore;
using uBoddy.Core.Models;

namespace uBoddy.Persistence
{
    public class UBoddyDBContext : DbContext
    {
        public DbSet<CategoryType> CategoryTypes { get; set; }
        public DbSet<Clinic> Clinics { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Treatment> Treatments { get; set; }
        public DbSet<TreatmentCategory> TreatmentCategories { get; set; }
        public DbSet<TreatmentSubcategory> TreatmentSubcategories { get; set; }



        public UBoddyDBContext(DbContextOptions<UBoddyDBContext> options)
           : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Clinic>().Property(c => c.CreatedTS).HasDefaultValueSql("getutcdate()");
            modelBuilder.Entity<Clinic>().Property(c => c.LastUpdatedTS).HasDefaultValueSql("getutcdate()");

            modelBuilder.Entity<Employee>().Property(e => e.CreatedTS).HasDefaultValueSql("getutcdate()");
            modelBuilder.Entity<Employee>().Property(e => e.LastUpdatedTS).HasDefaultValueSql("getutcdate()");

            modelBuilder.Entity<Treatment>().Property(t => t.CreatedTS).HasDefaultValueSql("getutcdate()");
            modelBuilder.Entity<Treatment>().Property(t => t.LastUpdatedTS).HasDefaultValueSql("getutcdate()");

        }
    }
}