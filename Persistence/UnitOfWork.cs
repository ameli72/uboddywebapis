using System.Threading.Tasks;
using uBoddy.Core;

namespace uBoddy.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly UBoddyDBContext context;
        public UnitOfWork(UBoddyDBContext context)
        {
            this.context = context;
        }

        public async Task CompleteAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}