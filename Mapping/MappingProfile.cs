using AutoMapper;
using uBoddy.Controllers.Resources;
using uBoddy.Core.Models;

namespace uBoddy.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            CreateMap<Clinic, ClinicResource>();
            CreateMap<Employee, EmployeeResource>();
            CreateMap<TreatmentCategory, TreatmentResource>();

            CreateMap<TreatmentSubcategory, TreatmentSubcategoryResource>();
            CreateMap<TreatmentCategory, TreatmentCategoryResource>();
                // .ForMember(dest => dest.CategoryType.Id, opt => opt.MapFrom(src => src.CategoryTypeId))
                // .ForMember(dest => dest.CategoryType.Name, opt => opt.MapFrom(src => src.Location.Name));
            CreateMap<CategoryType, KeyValuePairResource>();
        }
    }
}