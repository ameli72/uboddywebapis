using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace uBoddy.Core.Models
{
    public class Treatment
    {
        [Key]
        [Column("TreatmentId")]
        public int Id { get; set; }

        [Required]
        public DateTime CreatedTS { get; set; }
        [Required]
        public DateTime LastUpdatedTS { get; set; }


        public Clinic Clinic { get; set; }
        public int ClinicId { get; set; }

        public TreatmentSubcategory TreatmentSubcategory { get; set; }
        public int TreatmentSubcategoryId { get; set; }


        public uint MaxClients { get; set; }


        [Required]
        [MaxLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength(512)]
        public string Description { get; set; }

        public bool Deactivated { get; set; }
    }
}