using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace uBoddy.Core.Models
{
    public class Clinic
    {
        [Key]
        [Column("ClinicId")]
        public int Id { get; set; }

        [Required]
        public DateTime CreatedTS { get; set; }
        [Required]
        public DateTime LastUpdatedTS { get; set; }


        [Required]
        [MaxLength(128)]
        public string BusinessName { get; set; }

        [Required]
        [MaxLength(16)]
        public string CorporateId { get; set; }

        [Required]
        [MaxLength(32)]
        public string PhoneNumber { get; set; }


        public Byte[] CoverImage { get; set; }

        public Byte[] Logo { get; set; }



        [MaxLength(1024)]
        public string Intro { get; set; }

        public bool Deactivated { get; set; }



        public ICollection<Employee> Employees { get; set; }


        public Clinic() {
            Employees = new Collection<Employee>();
        }

    }
}