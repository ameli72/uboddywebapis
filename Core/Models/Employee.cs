using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace uBoddy.Core.Models
{
    //TODO: Do employees have mobile numbers?
    public class Employee
    {
        [Key]
        [Column("EmployeeId")]
        public int Id { get; set; }

        [Required]
        public DateTime CreatedTS { get; set; }
        [Required]
        public DateTime LastUpdatedTS { get; set; }


        [Required]
        [MaxLength(256)]
        public string EmailAddress { get; set; }
        [Required]
        [MaxLength(256)]
        public string Password { get; set; }
        public bool IsTemporaryPassword { get; set; }


        [Required]
        [MaxLength(128)]
        public string FullName { get; set; }

        public bool Administrator { get; set; }

        [MaxLength(16)]
        public string UBoddyId { get; set; }

        public Byte[] ProfilePicture { get; set; }

        public bool Deactivated { get; set; }



        public Clinic Clinic { get; set; }
        public int ClinicId { get; set; }


        [NotMapped]
        public string Token { get; set; }
    }
}