using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace uBoddy.Core.Models
{
    public class TreatmentSubcategory
    {
        [KeyAttribute()]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("TreatmentSubcategoryId")]
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }



        public TreatmentCategory TreatmentCategory { get; set; }
        public int TreatmentCategoryId { get; set; }
    }
}