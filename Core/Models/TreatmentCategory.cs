using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace uBoddy.Core.Models
{
    public class TreatmentCategory
    {
        [KeyAttribute()]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("TreatmentCategoryId")]
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }



        public CategoryType CategoryType { get; set; }
        public int CategoryTypeId { get; set; }


        public ICollection<TreatmentSubcategory> TreatmentSubcategories { get; set; }

    }
}