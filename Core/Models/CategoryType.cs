using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace uBoddy.Core.Models
{
    public class CategoryType
    {
        [KeyAttribute()]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("CategoryTypeId")]
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        public string Name { get; set; }




        public ICollection<TreatmentCategory> TreatmentCategories { get; set; }
    }
}