using System.Threading.Tasks;

namespace uBoddy.Core
{
    public interface IUnitOfWork
    {
         Task CompleteAsync();
    }
}