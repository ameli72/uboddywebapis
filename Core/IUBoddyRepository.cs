using System.Threading.Tasks;
using uBoddy.Controllers;
using uBoddy.Core.Models;

namespace uBoddy.Core
{
    public interface IUBoddyRepository
    {
        Task<Employee> Login(EmployeeLoginRequest request);
        // Task<bool> ForgotPassword(ForgotPasswordRequest request);
    }
}